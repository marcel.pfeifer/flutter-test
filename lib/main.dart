import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(home: Home()));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Flutter Test',
          style: TextStyle(color: Colors.black),
        ),
        toolbarOpacity: 0.6,
        backgroundColor: Colors.white,
      ),
      body: Center(
        child: Text(
          'Das ist ein Test! klappt das alles? Wäre schon echt mega cool',
          style: TextStyle(fontSize: 22, fontFamily: 'FiraCode'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Center(
          child: Text(
            '+',
            style: TextStyle(fontSize: 40),
          ),
        ),
      ),
    );
  }
}
